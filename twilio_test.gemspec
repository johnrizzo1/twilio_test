# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'twilio_test/version'

Gem::Specification.new do |spec|
  spec.name          = 'twilio_test'
  spec.version       = TwilioTest::VERSION
  spec.authors       = ['John Rizzo']
  spec.email         = ['johnrizzo1@gmail.com']
  spec.summary       = %q{This is an app that will sms me upon successful host ping.}
  spec.description   = %q{This is an app}
  spec.homepage      = 'http://johnrizzo.net'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.6'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'twilio-ruby', '~> 3.11', '>= 3.11.5'
  spec.add_development_dependency 'rufus-scheduler', '~> 3.0', '>= 3.0.8'
  spec.add_development_dependency 'net-ping', '~> 1.7', '>= 1.7.4'
end
