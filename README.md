# TwilioTest

This is a simple program that tests if a URL is available and txt's you if it isn't

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'twilio_test'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install twilio_test

## Usage

Set environment variables such as the following;

###Get this from your twilio account
    $ export ACCOUNT_SID='...'
    $ export AUTH_TOKEN='...'

### This is your twilio phone number
    $ export TWILIO_SENDER='+...'

### This is who you want to send the message to 
    $ export TWILIO_RECEIVER='+...'

### The host to test
 
    $ export IP_OR_FQDN='http://google.com'

### If you update the pingtest script you can set a cron like job based on rufus-scheduler  
    $ export TEST_INTERVAL='1m'

## Contributing

1. Fork it ( https://github.com/johnrizzo1/twilio_test/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
