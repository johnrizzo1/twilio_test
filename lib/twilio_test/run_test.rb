module TwilioTest
  require 'twilio-ruby'
  require 'rufus-scheduler'
  require 'net/ping'

  class RunTest
    def run_scheduled_ping_test(ip_or_fqdn, from, key, account_sid, auth_token, interval)
      scheduler = Rufus::Scheduler.new
      scheduler.every interval do
        ping_test ip_or_fqdn, from, key, account_sid, auth_token
      end
      scheduler.join
    end

    def run_single_ping_test(ip_or_fqdn, from, key, account_sid, auth_token)
      ping_test ip_or_fqdn, from, key, account_sid, auth_token
    end

    def run_single_web_test(ip_or_fqdn, from, key, account_sid, auth_token)
      web_test ip_or_fqdn, from, key, account_sid, auth_token
    end

    private
    def web_test(ip_or_fqdn, from, key, account_sid, auth_token)
      np = Net::Ping::HTTP.new(ip_or_fqdn)
      send_sms_if_ping(account_sid, auth_token, from, ip_or_fqdn, key, np)
    end

    def ping_test(ip_or_fqdn, from, key, account_sid, auth_token)
      np = Net::Ping::UDP.new(ip_or_fqdn)
      send_sms_if_ping(account_sid, auth_token, from, ip_or_fqdn, key, np)
    end

    def send_sms_if_ping(account_sid, auth_token, from, ip_or_fqdn, key, np)
      if np.ping?
        puts "#{ip_or_fqdn} is online!!!"
      else
        send_sms from, key, account_sid, auth_token, "#{ip_or_fqdn} is offline!!!"
      end
    end

    def send_sms(from, key, account_sid, auth_token, msg)
      client = Twilio::REST::Client.new account_sid, auth_token
      client.account.messages.create(
        :from => from,
        :to   => key,
        :body => msg
      )
    end
  end
end